<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\MajorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('all_data',[ApiController::class,'index']);
Route::get('/', function () {
    return view('welcome');
});
Route::get('get_data',[ApiController::class,'getData']);
Route::get('major_code',[ApiController::class,'majorCode']);
Route::get('maj_code/{id}',[ApiController::class,'majCode']);
Route::get('min_code/{id}',[ApiController::class,'minCode']);
Route::get('sub_min_code/{id}',[ApiController::class,'subMinCode']);
Route::get('drop',[ApiController::class,'drop']);
Route::post('add_member',[ApiController::class,'addMember']);
Route::post('add_organization',[ApiController::class,'addOrganization']);
Route::post('add_designation',[ApiController::class,'addDesignation']);
Route::get('all_organization',[ApiController::class,'allOrganization']);
Route::get('all_designation',[ApiController::class,'allDesignation']);
Route::get('all_member',[ApiController::class,'allMember']);
Route::post('import',[ApiController::class,'import']);
Route::get('export', [ApiController::class,'export']);
Route::get('edit_member/{id}',[ApiController::class,'editMember']);
Route::post('update_member',[ApiController::class,'updateMember']);
Route::get('delete_designation/{id}',[ApiController::class,'deleteDesignation']);
Route::get('delete_organization/{id}',[ApiController::class,'deleteOrganization']);
Route::get('delete_member/{id}',[ApiController::class,'deleteMember']);
Route::get('edit_organization/{id}',[ApiController::class,'editOrganization']);
Route::post('update_organization',[ApiController::class,'updateOrganization']);
Route::get('edit_designation/{id}',[ApiController::class,'editDesignation']);
Route::post('update_designation',[ApiController::class,'updateDesignation']);
Route::get('delete_db',[ApiController::class,'deleteDataBase']);
Route::post('allotment_entry',[ApiController::class,'allotmentEntry']);
Route::post('utilization_entry',[ApiController::class,'utilizationEntry']);
//
// Route::get('all_major_code',[ApiController::class,'allMajorCode']);
//
Route::get('all_sub_major_code',[ApiController::class,'allSubMajorCode']);
//
Route::get('all_minor_code',[ApiController::class,'allMinorCode']);
//
Route::get('all_sub_minor_code',[ApiController::class,'allSubMinor']);
//
Route::post('add_major_code',[ApiController::class,'addMajorCode']);
//
Route::post('add_sub_major_code',[ApiController::class,'addSubMajorCode']);
//
Route::post('add_minor_code',[ApiController::class,'addMinorCode']);
//
Route::post('add_sub_minor_code',[ApiController::class,'addSubMinorCode']);
//
Route::get('delete_major_code/{id}',[ApiController::class,'deleteMajorCode']);
//
Route::get('delete_sub_major_code/{id}',[ApiController::class,'deleteSubMajorCode']);
//
Route::get('delete_minor_code/{id}',[ApiController::class,'deleteMinorCode']);
//
Route::get('delete_sub_minor_code/{id}',[ApiController::class,'deleteSubMinorCode']);
//
Route::get('edit_Master_major/{id}',[ApiController::class,'editMasterMajor']);
Route::post('update_master_major',[ApiController::class,'updateMasterMajor']);
//
Route::get('edit_master_submajor/{id}',[ApiController::class,'editSubMasterMajor']);
Route::post('update_master_submajor',[ApiController::class,'updateSubMasterMajor']);
//
Route::get('edit_master_minor/{id}',[ApiController::class,'editMasterMinor']);
Route::post('update_master_minor',[ApiController::class,'updateMasterMinor']);
//
Route::get('edit_master_subminor/{id}',[ApiController::class,'editMasterSubMinor']);
Route::post('update_master_subminor',[ApiController::class,'updateMasterSubMinor']);
//
Route::get('major_alloted',[ApiController::class,'majorAllotedAmount']);
Route::get('sub_major_alloted',[ApiController::class,'subMajorAllotedAmount']);
Route::get('minor_alloted',[ApiController::class,'minorAllotedAmount']);
Route::get('sub_minor_alloted',[ApiController::class,'subMinorAllotedAmount']);
Route::get('percent_allot/{range}',[ApiController::class,'percentAllot']);
Route::get('percent_sub_major_allot/{range}',[ApiController::class,'percentAllotSubMajor']);
Route::get('percent_minor_allot/{range}',[ApiController::class,'percentAllotMinor']);
Route::get('percent_sub_minor_allot/{range}',[ApiController::class,'percentAllotMinor']);
Route::post('advance_search',[ApiController::class,'advanceSearch']);
Route::post('all_codes_data',[ApiController::class,'allCodeData']);
Route::get('allotment_entry_data',[ApiController::class,'allotmentTable']);
Route::get('edit_allotment_entry/{id}',[ApiController::class,'editAllotmentEntry']);
Route::get('delete_allot_entry/{id}',[ApiController::class,'deleteEntry']);
//
Route::get('all_object_code',[ApiController::class,'objectheadcodes']);

// MajorController Routes
Route::get('all_m_code',[MajorController::class,'allMajorCode']);
Route::get('sm_code/{code}',[MajorController::class,'allSubMajorCode']);
Route::get('mi_code/{code}',[MajorController::class,'allMinorCode']);
Route::get('s_mi_code/{code}',[MajorController::class,'allSubMinorCode']);
Route::get('all_object_head/{code}',[MajorController::class,'allObjectHead']);
Route::post('all_allot_exp',[MajorController::class,'searchCodes']);
Route::post('search_by_object_head_table',[MajorController::class,'advanceFilter']);
Route::post('search_by_treasury',[MajorController::class,'advanceFilter2']);
Route::get('major_trim/{mc}',[MajorController::class,'majorTrim']);
// Route::get('sub_major_trim/{mc}/{smc}',[MajorController::class,'subMajorTrim']);
//advance filter api
Route::post('advance1',[MajorController::class,'advanceFilter1']);
Route::post('advance2',[MajorController::class,'advanceFilter3']);
Route::post('advance3',[MajorController::class,'advanceFilter4']);
Route::post('advance4',[MajorController::class,'advanceFilter5']);
Route::post('advance5',[MajorController::class,'advanceFilter6']);
//get district
Route::get('get_district',[MajorController::class,'getDistrict']);
Route::get('district/{district}',[MajorController::class,'dataByDistrict']);
Route::get('all_treasury/{district}',[MajorController::class,'districtTreasury']);
Route::post('search_treasury',[MajorController::class,'searchTreasury']);
Route::get('get_office_level/{treasury}',[MajorController::class,'officeLevel']);
Route::post('get_office_id',[MajorController::class,'officeId']);
Route::post('search_district',[MajorController::class,'searchDistrict']);
Route::post('search_with_treasury',[MajorController::class,'searchByTreasury']);
Route::post('search_office_level',[MajorController::class,'searchOfficeLevel']);
Route::post('treasury_filter',[MajorController::class,'filterTreasury']);
Route::post('office_level_filter',[MajorController::class,'filterOfficeLevel']);
Route::post('office_id_filter',[MajorController::class,'filterOfficeId']);
