<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllotmentEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allotment_entry', function (Blueprint $table) {
            $table->id();
            $table->string('ref_no')->nullable();
            $table->string('date')->nullable();
            $table->string('amount')->nullable();
            $table->string('budget_date')->nullable();
            $table->string('budget_line')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allotment_entry');
    }
}
