/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from "vue";
import VueRouter from "vue-router";
import Dropdown from 'vue-simple-search-dropdown';
import router from "./routes";
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueSweetalert2 from 'vue-sweetalert2';
import Swal from 'sweetalert2';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import '../../resources/css/app.css';
import vSelect from "vue-select";
import jquery from 'jquery';

require('./bootstrap');
import VueApexCharts from 'vue-apexcharts';
// import FileSaver from 'file-saver';
Vue.use(VueApexCharts)

Vue.component("v-select", vSelect);
Vue.component('apexchart', VueApexCharts);
Vue.use(VueRouter);
  Vue.use(Dropdown);
// Vue.use(FileSaver);
Vue.use(router);
Vue.use(VueSweetalert2);
window.Swal = Swal;
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(jquery);

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
// Vue.component('login-component', require('./components/LoginComponent.vue').default);

/** 
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});