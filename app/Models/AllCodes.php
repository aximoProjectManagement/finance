<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllCodes extends Model
{
    use HasFactory;
    protected $table = 'all_codes';
}
