<?php


namespace App\Imports;
ini_set('memory_limit','16M');

use App\ExcelImport;        
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\AllotmentExp;

class UsersImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new AllotmentExp([
            'allotted_by'   =>  $row['allotted_by'],
            'allotted_to'   =>  $row['allotted_to'],
            'office_id' =>  $row['office_id'],
            'office_level'  =>  $row['office_level'],
            'd_no'          =>  $row['d_no'],
            'major_code'    =>  $row['major_code'],
            'sub_major_code'    =>  $row['sub_major_code'],
            'minor_code'    =>  $row['minor_code'],
            'sub_minor_code' => $row['sub_minor_code'],
            'object_head'   =>  $row['object_head'],
            'full_code'     =>  $row['full_code'],
            'hoa_id'        =>  $row['hoa_id'],
            'ddo_code'      =>  $row['ddo_code'],
            'treasury'      =>  $row['treasury'],
            'district'      =>  $row['district'],
            'allotted_amount' =>    $row['allotted_amount'],
            'available_amount' =>   $row['available_amount'],
            'expenditure_amount' => $row['expenditure_amount']

        ]);
    }
}
