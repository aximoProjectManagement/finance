<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'SuperAdmin',
            'email' => 'admin@finance.com',
            'password' => bcrypt('123456'),
            'email_verified_at' => now(),
            'role_id' => 1
        ]);
    }
}
