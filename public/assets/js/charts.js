
window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer", {
        title: {
            text: ""
        },
        axisY: [{
            title: "Order",
            lineColor: "#C24642",
            tickColor: "#C24642",
            labelFontColor: "#C24642",
            titleFontColor: "#C24642",
            includeZero: true,
            suffix: "k"
        },
        {
            title: "Footfall",
            lineColor: "#369EAD",
            tickColor: "#369EAD",
            labelFontColor: "#369EAD",
            titleFontColor: "#369EAD",
            includeZero: true,
            suffix: "k"
        }],
        axisY2: {
            title: "Revenue",
            lineColor: "#7F6084",
            tickColor: "#7F6084",
            labelFontColor: "#7F6084",
            titleFontColor: "#7F6084",
            includeZero: true,
            prefix: "$",
            suffix: "k"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "line",
            name: "Footfall",
            color: "#369EAD",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { x: new Date(2017, 00, 7), y: 85.4 },
                { x: new Date(2017, 00, 14), y: 92.7 },
                { x: new Date(2017, 00, 21), y: 64.9 },
                { x: new Date(2017, 00, 28), y: 58.0 },
                { x: new Date(2017, 01, 4), y: 63.4 },
                { x: new Date(2017, 01, 11), y: 69.9 },
                { x: new Date(2017, 01, 18), y: 88.9 },
                { x: new Date(2017, 01, 25), y: 66.3 },
                { x: new Date(2017, 02, 4), y: 82.7 },
                { x: new Date(2017, 02, 11), y: 60.2 },
                { x: new Date(2017, 02, 18), y: 87.3 },
                { x: new Date(2017, 02, 25), y: 98.5 }
            ]
        },
        {
            type: "line",
            name: "Order",
            color: "#C24642",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { x: new Date(2017, 00, 7), y: 32.3 },
                { x: new Date(2017, 00, 14), y: 33.9 },
                { x: new Date(2017, 00, 21), y: 26.0 },
                { x: new Date(2017, 00, 28), y: 15.8 },
                { x: new Date(2017, 01, 4), y: 18.6 },
                { x: new Date(2017, 01, 11), y: 34.6 },
                { x: new Date(2017, 01, 18), y: 37.7 },
                { x: new Date(2017, 01, 25), y: 24.7 },
                { x: new Date(2017, 02, 4), y: 35.9 },
                { x: new Date(2017, 02, 11), y: 12.8 },
                { x: new Date(2017, 02, 18), y: 38.1 },
                { x: new Date(2017, 02, 25), y: 42.4 }
            ]
        },
        {
            type: "line",
            name: "Revenue",
            color: "#7F6084",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: new Date(2017, 00, 7), y: 42.5 },
                { x: new Date(2017, 00, 14), y: 44.3 },
                { x: new Date(2017, 00, 21), y: 28.7 },
                { x: new Date(2017, 00, 28), y: 22.5 },
                { x: new Date(2017, 01, 4), y: 25.6 },
                { x: new Date(2017, 01, 11), y: 45.7 },
                { x: new Date(2017, 01, 18), y: 54.6 },
                { x: new Date(2017, 01, 25), y: 32.0 },
                { x: new Date(2017, 02, 4), y: 43.9 },
                { x: new Date(2017, 02, 11), y: 26.4 },
                { x: new Date(2017, 02, 18), y: 40.3 },
                { x: new Date(2017, 02, 25), y: 54.2 }
            ]
        }]
    });

    // chart 2
    {

        var chart2 = new CanvasJS.Chart("chartContainer2", {
            animationEnabled: true,
            title: {
                text: ""
            },
            axisY: {
                title: "Medals",
                includeZero: true
            },
            legend: {
                cursor: "pointer",
                itemclick: toggleDataSeries
            },
            toolTip: {
                shared: true,
                content: toolTipFormatter
            },
            data: [{
                type: "bar",
                showInLegend: true,
                name: "Gold",
                color: "gold",
                dataPoints: [
                    { y: 243, label: "Italy" },
                    { y: 236, label: "China" },
                    { y: 243, label: "France" },
                    { y: 273, label: "Great Britain" },
                    { y: 269, label: "Germany" },
                    { y: 196, label: "Russia" },
                    { y: 1118, label: "USA" }
                ]
            },
            {
                type: "bar",
                showInLegend: true,
                name: "Silver",
                color: "silver",
                dataPoints: [
                    { y: 212, label: "Italy" },
                    { y: 186, label: "China" },
                    { y: 272, label: "France" },
                    { y: 299, label: "Great Britain" },
                    { y: 270, label: "Germany" },
                    { y: 165, label: "Russia" },
                    { y: 896, label: "USA" }
                ]
            },
            {
                type: "bar",
                showInLegend: true,
                name: "Bronze",
                color: "#A57164",
                dataPoints: [
                    { y: 236, label: "Italy" },
                    { y: 172, label: "China" },
                    { y: 309, label: "France" },
                    { y: 302, label: "Great Britain" },
                    { y: 285, label: "Germany" },
                    { y: 188, label: "Russia" },
                    { y: 788, label: "USA" }
                ]
            }]
        });


        function toolTipFormatter(e) {
            var str = "";
            var total = 0;
            var str3;
            var str2;
            for (var i = 0; i < e.entries.length; i++) {
                var str1 = "<span style= \"color:" + e.entries[i].dataSeries.color + "\">" + e.entries[i].dataSeries.name + "</span>: <strong>" + e.entries[i].dataPoint.y + "</strong> <br/>";
                total = e.entries[i].dataPoint.y + total;
                str = str.concat(str1);
            }
            str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
            str3 = "<span style = \"color:Tomato\">Total: </span><strong>" + total + "</strong><br/>";
            return (str2.concat(str)).concat(str3);
        }



    }
    //chart3

    {

        var chart3 = new CanvasJS.Chart("chartContainer3", {
            animationEnabled: true,
            title: {
                text: ""
            },
            data: [{
                type: "pie",
                startAngle: 240,
                yValueFormatString: "##0.00\"%\"",
                indexLabel: "{label} {y}",
                dataPoints: [
                    {y: 79.45, label: "Google"},
                    {y: 7.31, label: "Bing"},
                    {y: 7.06, label: "Baidu"},
                    {y: 4.91, label: "Yahoo"},
                    {y: 1.26, label: "Others"}
                ]
            }]
        });
       
        
        }
    
    chart.render();
    chart2.render();
    chart3.render();

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
        e.chart2.render();
        e.chart3.render();
    }

}
