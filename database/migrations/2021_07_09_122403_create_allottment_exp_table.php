<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllottmentExpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allottment_exp', function (Blueprint $table) {
            $table->id();
            $table->string("allotted_by")->nullable();
            $table->string("allotted_to")->nullable();
            $table->string("office_id")->nullable();
            $table->string("office_level")->nullable();
            $table->string("d_no")->nullable();  
            $table->string("major_code")->nullable();
            $table->string("sub_major_code")->nullable();
            $table->string("minor_code")->nullable();
            $table->string("sub_minor_code")->nullable();
            $table->string("object_head")->nullable();
            $table->string("full_code")->nullable();
            $table->string("hoa_id")->nullable();
            $table->string("ddo_code")->nullable();
            $table->string("treasury")->nullable();
            $table->string("district")->nullable();
            $table->string("allotted_amount")->nullable();
            $table->string("available_amount")->nullable();
            $table->string("expenditure_amount")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allottment_exp');
    }
}
