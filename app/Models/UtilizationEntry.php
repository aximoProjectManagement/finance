<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UtilizationEntry extends Model
{
    use HasFactory;
    protected $table = 'utilization_entry';
}
