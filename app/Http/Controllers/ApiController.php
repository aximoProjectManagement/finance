<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AllotmentExp;
use App\Models\SubMinorCode;
use App\Models\MajorCode;
use App\Models\MinorCode;
use App\Models\SubMajorCode;
use App\Models\Code;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use App\Exports\UsersExport;
use Hash;
use App\Models\User;
use App\Models\Organization;
use App\Models\Designation;
use App\Models\AllotmentEntry;
use App\Models\UtilizationEntry;
use App\Models\AllCodes;
use App\Models\ObjectHead;
use DB;

class ApiController extends Controller
{
    public function objectheadcodes(){
        $data = DB::table('object_head')->get();
        return $data;
    }
    public function index(Request $request){
        $search = request()->get('search');
        $ret = DB::table('allottment_exp')->where("full_code","LIKE","%{$search}%")->paginate(10);
        return response()->json(
            $ret
        );
    }
    public function getData(){
        $data = DB::table('allottment_exp')->paginate();
        return $data;
    }

    public function allMajorCode(){
        $data = MajorCode::get();
        return response()->json([
            'data'  => $data,
            'status'    => 'Success'
        ]);
    }
    
    public function majorCode(){
       $data = SubMinorCode::with(['minorcode','submajor','majorcode'])->get();
       return response()->json([
           'subminor'   => $data,
       ]);
    }
    public function majCode(Request $request){
        $data = DB::table('sub_major_code')->where('major_code_id',$request->id)->get();
        return response()->json(
            $data
        );
    }
    public function allSubMajorCode(){
        $data = DB::table('sub_major_code')->get();
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function minCode(Request $request){
        $data = MinorCode::where('sub_major_id',$request->id)->get();
        return response()->json(
            $data
        );
    }
    public function allMinorCode(){
        $data = MinorCode::get();
        return response()->json([
            'data'  => $data,
            'status'    => 'success'
        ]);
    }
    public function subMinCode(Request $request){
        $data = SubMinorCode::where('minor_code_id',$request->id)->get();
        return response()->json(
            $data
        );
    }
    public function allSubMinor(){
        $data = SubMinorCode::get();
        return response()->json([
            'data' => $data,
            'status'    => 'success'
        ]);
    }
    public function addMember(Request $request){
        $data = new User;
        $data->name = $request->input('name');
        $data->mobile = $request->input('mobile');
        $data->email = $request->input('email');
        $data->role_id = $request->input('role');
        $data->organization = $request->input('organization');
        $data->designation = $request->input('designation');
        $data->password = Hash::make($request->input('password'));
        $data->save();
        return response()->json([
            'data' => $data,
            'status' => 'success'
        ]);

    }
    public function addOrganization(Request $request){
        $data = new Organization;
        $data->organization = $request->input('organization');
        $data->save();
        return response()->json([
            'data' => $data,
            'status' => 'success'
        ]);
    }
    public function addDesignation(Request $request){
        $data = new Designation;
        $data->organization = $request->input('organization');
        $data->designation = $request->input('designation');

        $data->save();
        return response()->json([
            'data' => $data,
            'status' => 'success'
        ]);
    }
    public function allOrganization(){
        $data = Organization::get();
        return response()->json([
            'data' => $data,
            'status' => 'success'
        ]);
    }
    public function allDesignation(){
        $data = Designation::get();
        return response()->json([
            'data' => $data,
            'status' => 'success'
        ]);
    }
    public function allMember(){
        $data = User::get();
        return response()->json([
            'data' => $data
        ]);
    }
    public function import(Request $request){
        $request->validate([
            'select_excel_file' => 'required|file|mimes:xls,xlsx'
        ]);
        $data = Excel::import(new UsersImport,request()->file('select_excel_file'));
        

        return response()->json(['message' => 'uploaded successfully'], 200);
    }
    public function editMember($id){
        $data = User::find($id);
        return response()->json([
            'data' => $data,
            'status' => 'success'
        ]);
    }
    public function updateMember(Request $request){
        $id = $request->id;
        $data = User::where('id',$id)->first();
        $ret = $data->update([
            'role_id'       =>  $request->input('role_id'),
            'name'          =>  $request->input('name'),
            'mobile'        =>  $request->input('mobile'),
            'email'         =>  $request->input('email'),
            'designation'  =>  $request->input('designation'),
            'organization'   =>  $request->input('organization'),
            'password'      => Hash::make($request->password)
        ]);
        if($ret){
            return response()->json([
                'data'      =>  $data,
                'message'   =>  'Updated Successfully!',
                'status'    =>  'Success'
            ]);
        }
    }
    public function deleteDesignation($id){
        $data = Designation::find($id);
        $ret = $data->delete();
        return response()->json([
            'data'  => $ret,
            'status'    => 'success'
        ]);
    }

    public function deleteOrganization($id){
        $data = Organization::find($id);
        $ret = $data->delete();
        return response()->json([
            'data'  => $ret,
            'status'    => 'success'
        ]);
    }
    public function deleteMember($id){
        $data = User::find($id);
        $ret = $data->delete();
        return response()->json([
            'data'  => $ret,
            'status'    => 'success'
        ]);
    }
    public function editOrganization(Request $request, $id){
        $data = Organization::find($id);
        return response()->json([
            'data'      =>  $data,
            'status'    => 'success'
        ]);
    }
    public function updateOrganization(Request $request){
        $id = $request->id;
        $data = Organization::where('id',$id)->first();
        $ret = $data->update([
            'organization'  =>  $request->organization
        ]);
        if($ret){
            return response()->json([
                'data'      =>  $ret,
                'message'   =>  'Updated Successfully!',
                'status'    =>  'Success'
            ]);
        }
    }
    public function editDesignation(Request $request, $id){
        $data = Designation::find($id);
        return response()->json([
            'data'      =>  $data,
            'status'    => 'success'
        ]);
    }
    public function updateDesignation(Request $request){
        $id = $request->id;
        $data = Designation::where('id',$id)->first();
        $ret = $data->update([
            'designation'  =>  $request->designation,
            'organization'   =>  $request->organization
        ]);
        if($ret){
            return response()->json([
                'data'      =>  $ret,
                'message'   =>  'Updated Successfully!',
                'status'    =>  'Success'
            ]);
        }
    }
    public function export(){
        Excel::download(new UsersExport, 'finance.xlsx');
    }
    
    public function deleteDataBase(){
        $data = AllotmentExp::truncate();
        return response()->json([
            'status'    =>  'success',
            'message'   =>  'Database Clear Successfully'
        ]);
    }
    public function allotmentEntry(Request $request){
        $data = new AllotmentEntry;
        $data->ref_no = $request->input('ref_no');
        $data->date = $request->input('date');
        $data->amount = $request->input('amount');
        $data->letter_from = $request->input('letter_from');
        $data->letter_to = $request->input('letter_to');
        $data->budget_line = $request->input('budget_line');
        $data->save();
        return response()->json([
            'data'  =>  $data,
            'status'    => 'success'
        ]);
    }
    public function utilizationEntry(Request $request){
        $data = new UtilizationEntry;
        $data->ref_no = $request->input('ref_no');
        $data->date = $request->input('date');
        $data->amount = $request->input('amount');
        $data->budget_date = $request->input('budget_date');
        $data->budget_line = $request->input('budget_line');
        $data->utilization_certificate = $request->input('utilization_certificate');
        $data->save();
        return response()->json([
            'data'  =>  $data,
            'status'    => 'success'
        ]);
    }
    public function addMajorCode(Request $request){
        $data = new MajorCode;
        $data->code = $request->input('code');
        $data->description = $request->input('description');
        $data->save();
        return response()->json([
            'data'  =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function addSubMajorCode(Request $request){
        $data = new SubMajorCode;
        $data->major_code_id = $request->input('major_code_id');
        $data->code = $request->input('code');
        $data->description = $request->input('description');
        $data->save();
        return response()->json([
            'data'  =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function addMinorCode(Request $request){
        $data = new MinorCode;
        $data->sub_major_id = $request->input('major_code_id');
        $data->code = $request->input('code');
        $data->description = $request->input('description');
        $data->save();
        return response()->json([
            'data'  =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function addSubMinorCode(Request $request){
        $data = new SubMinorCode;
        $data->minor_code_id = $request->input('minor_code_id');
        $data->code = $request->input('code');
        $data->description = $request->input('description');
        $data->save();
        return response()->json([
            'data'  =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function deleteMajorCode($id){
        $data = MajorCode::find($id);
        $ret = $data->delete();
        return response()->json([
            'data'  => $ret,
            'status'    => 'success'
        ]);
    }
    public function deleteSubMajorCode($id){
        $data = SubMajorCode::find($id);
        $ret = $data->delete();
        return response()->json([
            'data'  => $ret,
            'status'    => 'success'
        ]);
    }
    public function deleteMinorCode($id){
        $data = MinorCode::find($id);
        $ret = $data->delete();
        return response()->json([
            'data'  => $ret,
            'status'    => 'success'
        ]);
    }
    public function deleteSubMinorCode($id){
        $data = SubMinorCode::find($id);
        $ret = $data->delete();
        return response()->json([
            'data'  => $ret,
            'status'    => 'success'
        ]);
    }
    //edit and update major code
    public function editMasterMajor(Request $request, $id){
        $data = MajorCode::find($id);
        return response()->json([
            'data'      =>  $data,
            'status'    => 'success'
        ]);
    }
    public function updateMasterMajor(Request $request){
        $id = $request->id;
        $data = MajorCode::where('id',$id)->first();
        $ret = $data->update([
            'code'  =>  $request->code,
            'description'   =>  $request->description
        ]);
        if($ret){
            return response()->json([
                'data'      =>  $ret,
                'message'   =>  'Updated Successfully!',
                'status'    =>  'Success'
            ]);
        }
    }
    //edit submajor code and update
    public function editSubMasterMajor(Request $request, $id){
        $data = SubMajorCode::find($id);
        return response()->json([
            'data'      =>  $data,
            'status'    => 'success'
        ]);
    }
    public function updateSubMasterMajor(Request $request){
        $id = $request->id;
        $data = SubMajorCode::where('id',$id)->first();
        $ret = $data->update([
            'major_code_id'  =>  $request->major_code_id,
            'code'           =>  $request->code,
            'description'   =>  $request->description
        ]);
        if($ret){
            return response()->json([
                'data'      =>  $ret,
                'message'   =>  'Updated Successfully!',
                'status'    =>  'Success'
            ]);
        }
    }
    //edit minor code and update
    public function editMasterMinor(Request $request, $id){
        $data = MinorCode::find($id);
        return response()->json([
            'data'      =>  $data,
            'status'    => 'success'
        ]);
    }
    public function updateMasterMinor(Request $request){
        $id = $request->id;
        $data = MinorCode::where('id',$id)->first();
        $ret = $data->update([
            'code'           =>  $request->code,
            'description'   =>  $request->description
        ]);
        if($ret){
            return response()->json([
                'data'      =>  $ret,
                'message'   =>  'Updated Successfully!',
                'status'    =>  'Success'
            ]);
        }
    }
    //edit sub minor code and update
    public function editMasterSubMinor(Request $request, $id){
        $data = SubMinorCode::find($id);
        return response()->json([
            'data'      =>  $data,
            'status'    => 'success'
        ]);
    }
    public function updateMasterSubMinor(Request $request){
        $id = $request->id;
        $data = SubMinorCode::where('id',$id)->first();
        $ret = $data->update([
            'code'           =>  $request->code,
            'description'   =>  $request->description
        ]);
        if($ret){
            return response()->json([
                'data'      =>  $ret,
                'message'   =>  'Updated Successfully!',
                'status'    =>  'Success'
            ]);
        }
    }
    public function majorAllotedAmount(Request $request){
        $code = MajorCode::get();
        $data = array();
        foreach ($code as $i){
            $allot_total= AllotmentExp::where('major_code',$i->code)->sum('allotted_amount');
            $i['total'] = $allot_total;
            $data[]= $i;
           
        }   
        
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function subMajorAllotedAmount(Request $request){
        $code = SubMajorCode::get();
        $data = array();
        foreach ($code as $i){
            $allot_total= AllotmentExp::where('sub_major_code',$i->code)->sum('allotted_amount');
            $i['total'] = $allot_total;
            $data[]= $i;
           
        }   
        
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function minorAllotedAmount(Request $request){
        $code = MinorCode::get();
        $data = array();
        foreach ($code as $i){
            $allot_total= AllotmentExp::where('minor_code',$i->code)->sum('allotted_amount');
            $i['total'] = $allot_total;
            $data[]= $i;
           
        }   
        
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function subMinorAllotedAmount(Request $request){
        $code = SubMinorCode::get();
        $data = array();
        foreach ($code as $i){
            $allot_total= AllotmentExp::where('sub_minor_code',$i->code)->sum('allotted_amount');
            $i['total'] = $allot_total;
            $data[]= $i;
           
        }   
        
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function percentAllot($range){
        $code = MajorCode::get();
        $data = array();
        $percent = array();
        foreach ($code as $i){
            $allot_total= AllotmentExp::where('major_code',$i->code)->sum('allotted_amount');
            $i['total'] = $allot_total;
            $total_percent = $allot_total + ($allot_total*($range/100));
            $i['percent_total'] = $total_percent;
            $data[]= $i;
           
        }   
        
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function percentAllotSubMajor($range){
        $code = SubMajorCode::get();
        $data = array();
        $percent = array();
        foreach ($code as $i){            
            $allot_total= AllotmentExp::where('sub_major_code',$i->code)->sum('allotted_amount');
            
            $i['total'] = $allot_total;
            $total_percent = $allot_total + ($allot_total*($range/100));
            $i['percent_total'] = $total_percent;
            $data[]= $i;
           
        }
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function percentAllotMinor($range){
        $code = MinorCode::get();
        $data = array();
        $percent = array();
        foreach ($code as $i){            
            $allot_total= AllotmentExp::where('minor_code',$i->code)->sum('allotted_amount');
            
            $i['total'] = $allot_total;
            $total_percent = $allot_total + ($allot_total*($range/100));
            $i['percent_total'] = $total_percent;
            $data[]= $i;
           
        }
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function percentAllotSubMinor($range){
        $code = SubMinorCode::get();
        $data = array();
        $percent = array();
        foreach ($code as $i){            
            $allot_total= AllotmentExp::where('sub_minor_code',$i->code)->sum('allotted_amount');
            
            $i['total'] = $allot_total;
            $total_percent = $allot_total + ($allot_total*($range/100));
            $i['percent_total'] = $total_percent;
            $data[]= $i;
           
        }
        return response()->json([
            'data'  => $data,
            'status' => 'success'
        ]);
    }
    public function advanceSearch(Request $request){
        $search = request()->get('search');
        $ret = DB::table('allottment_exp')
        ->where("full_code","LIKE","%{$search}%")
        ->orWhere("allotted_by","LIKE","%{$search}%")
        ->orWhere("allotted_to","LIKE","%{$search}%")->
        orWhere("district","LIKE","%{$search}%")->get();

        return response()->json(
            $ret
        );

    }
    public function allCodeData(){
        $search = request()->get('search');
        $data = DB::table('all_codes')
        ->where("description","LIKE","%{$search}%")->get();        
        return response()->json([
            'data'  =>  $data
        ]);
    }
    public function allotmentTable(){
        $data = AllotmentEntry::get();
        return response()->json([
            'data'  =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function editAllotmentEntry(Request $request, $id){
        $data = AllotmentEntry::find($id);
        return response()->json([
            'data'      =>  $data,
            'status'    => 'success'
        ]);
    }
    public function deleteEntry($id){
        $data = AllotmentEntry::find($id);
        $ret = $data->delete();
        return response()->json([
            'data'  => $ret,
            'status'    => 'success'
        ]);
    }
}
