<?php

namespace App\Exports;

use App\Models\AllotmentExp;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;

class UsersExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return AllotmentExp::all();
    }
    public function  headings(): array{
        return[
            'id',
            'allotted_by',
            'allotted_to',
            'office_id',
            'office_level',
            'd_no',
            'major_code',
            'sub_major_code',
            'minor_code',
            'sub_minor_code',
            'object_head',
            'full_code',
            'hoa_id',
            'ddo_code',
            'treasury',
            'district',
            'allotted_amount',
            'available_amount',
            'expenditure_amount',
            'created_at',
            'updated_at'
        ];
    }
}
