<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllotmentEntry extends Model
{
    use HasFactory;
    protected $table = 'allotment_entry';
}
