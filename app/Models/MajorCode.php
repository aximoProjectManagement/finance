<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MajorCode extends Model
{
    use HasFactory;
    protected $table = 'major_code';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code',
        'description'
    ];
    
}
