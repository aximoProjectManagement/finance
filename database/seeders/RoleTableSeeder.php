<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['id' => 1,'role' => 'SuperAdmin'],
            ['id' => 2,'role' => 'Admin'],
        ];

        \App\Models\Role::insert(
            $roles
        );
    }
}
