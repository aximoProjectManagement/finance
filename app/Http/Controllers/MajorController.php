<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ObjectHead;
use App\Models\AllotmentExp;

class MajorController extends Controller
{
    public function allMajorCode(){
        $data = ObjectHead::select('major_code','major_code_description','major_code_key')->distinct()->get();
        return response()->json([
            'data'      =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function allSubMajorCode($code){
        $data = ObjectHead::where('major_code_key',$code)->select('sub_major_code','sub_major_description','sub_major_code_key')->distinct()->get();
        return response()->json([
            'data'      =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function allMinorCode($code){
        $data = ObjectHead::where('sub_major_code_key',$code)->select('minor_code','minor_code_description','minor_code_key')->distinct()->get();
        return response()->json([
            'data'      =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function allSubMinorCode($code){
        $data = ObjectHead::where('minor_code_key',$code)->select('sub_minor_code','sub_minor_description','sub_minor_code_key')->distinct()->get();
        return response()->json([
            'data'      =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function allObjectHead($code){
        $data = ObjectHead::where('sub_minor_code_key',$code)->select('full_code','object_head_description','object_head')->distinct()->get();
        return response()->json([
            'data'      =>  $data,
            'status'    =>  'success'
        ]);
    }
    public function searchCodes(Request $request){
        $search = $request->get('search');
        $data = AllotmentExp::where("full_code","LIKE","%{$search}%")->paginate(10);
        $sumAvailable = AllotmentExp::where("full_code","LIKE","%{$search}%")->sum('available_amount');
        $sumAlloted = AllotmentExp::where("full_code","LIKE","%{$search}%")->sum('allotted_amount');
        $sumExpenditure = AllotmentExp::where("full_code","LIKE","%{$search}%")->sum('expenditure_amount');
        $total = array();
        foreach($data as $key=>$i){
            $total[] = $i;
        }

        return response()->json([
            'data' => $data,
            'sum_alloted' =>$sumAlloted,
            'sum_available' => $sumAvailable,
            'sumExpenditure' => $sumExpenditure
        ]);
    }
    public function advanceFilter1(Request $request){
        $search = $request->get("search");
        $data = ObjectHead::where("major_code_description","LIKE","%{$search}%")->select('major_code_description','major_code')->distinct()->get();
        return response()->json(
            $data
        );
    }
    public function advanceFilter3(Request $request){
        $search = $request->get("search");
        $data = ObjectHead::where("sub_major_description","LIKE","%{$search}%")->select('sub_major_description','sub_major_code')->distinct()->get();
        return response()->json(
            $data
        );
    }
    public function advanceFilter4(Request $request){
        $search = $request->get("search");
        $data = ObjectHead::where("minor_code_description","LIKE","%{$search}%")->select('minor_code_description','minor_code')->distinct()->get();
        return response()->json(
            $data
        );
    }
    public function advanceFilter5(Request $request){
        $search = $request->get("search");
        $data = ObjectHead::where("sub_minor_description","LIKE","%{$search}%")->select('sub_minor_description','sub_minor_code')->distinct()->get();
        return response()->json(
            $data
        );
    }
    public function advanceFilter6(Request $request){
        $search = $request->get("search");
        $data = ObjectHead::where("full_code","LIKE","%{$search}%")->select('full_code')->distinct()->get();
        return response()->json(
            $data
        );
    }
    public function majorTrim($mc){
        $data = AllotmentExp::select('allotted_amount','available_amount','expenditure_amount')->where('major_code',$mc)->get();
        return response()->json(
            $data
        );
    }
    public function advanceFilter2(Request $request){
        $search = $request->get('search');
        $data = AllotmentExp::where("treasury","LIKE","%{$search}%")->paginate(10);
        return response()->json(
            $data
        );
    }

    public function getDistrict(){
        $data = AllotmentExp::select('district')->distinct()->get();
        return response()->json(
            $data
        );
    }
    public function dataByDistrict($district){
        $data = AllotmentExp::where('district',$district)->paginate(10);
        $sumAlloted = AllotmentExp::select('allotted_amount')->where('district',$district)->sum('allotted_amount');
        $sumExpenditure = AllotmentExp::select('expenditure_amount')->where('district',$district)->sum('expenditure_amount');
        return response()->json([
            $data,
            $sumExpenditure,
            $sumAlloted
        ]);
    }
    public function districtTreasury($district){
        $data = AllotmentExp::where('district',$district)->select('treasury')->distinct()->get();
        return response()->json(
            $data
        );
    }
    public function searchTreasury(Request $request){
        $search = $request->get('search');
        $data = AllotmentExp::where("treasury","LIKE","%{$search}%")->paginate(10);
        $sumAlloted = AllotmentExp::select('allotted_amount')->where("treasury","LIKE","%{$search}%")->sum('allotted_amount');
        $sumExpenditure = AllotmentExp::select('expenditure_amount')->where("treasury","LIKE","%{$search}%")->sum('expenditure_amount');
        return response()->json([
            $data,
            $sumAlloted,
            $sumExpenditure
        ]);
    }
    public function officeLevel($treasury){
        $data = AllotmentExp::where('treasury',$treasury)->select('office_level')->distinct()->get();
        return response()->json([
            $data
        ]);
    }
    public function officeId(Request $request){
        $data = AllotmentExp::where('treasury',$request->treasury)->where('office_level',$request->office_level)->select('office_id')->distinct()->get();
        return response()->json([
            $data
        ]);
    }
    public function searchDistrict(Request $request){
       $search = $request->get('search');
       $data = AllotmentExp::where("district","LIKE","%{$search}%")->paginate(10);
       return response()->json([
           'data'   => $data
       ]);
    }
    public function searchByTreasury(Request $request){
        $search = $request->get('search');
        $data = AllotmentExp::where("treasury","LIKE","%{$search}%")->paginate(10);
        return response()->json([
            'data'   => $data
        ]);
     }
     public function searchOfficeLevel(Request $request){
        $search = $request->get('search');
        $data = AllotmentExp::where("office_level","LIKE","%{$search}%")
        ->orWhere("treasury","LIKE","%{$search}%")
        ->paginate(10);
        return response()->json([
            'data'   => $data
        ]);
     }
     public function filterTreasury(Request $request){
         $data = AllotmentExp::where('district',$request->district)->where('treasury',$request->treasury)->paginate(10);
         return response()->json([
             'data' =>  $data
         ]);
     }

     public function filterOfficeLevel(Request $request){
        $data = AllotmentExp::where('district',$request->district)->where('treasury',$request->treasury)->
        where('office_level',$request->office_level)->paginate(10);
        return response()->json([
            'data' =>  $data
        ]);
    }
    public function filterOfficeId(Request $request){
        $data = AllotmentExp::where('district',$request->district)->where('treasury',$request->treasury)->
        where('office_level',$request->office_level)->where('office_id',$request->office_id)->paginate(10);
        return response()->json([
            'data' =>  $data
        ]);
    }
}
