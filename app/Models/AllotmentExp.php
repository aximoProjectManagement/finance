<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllotmentExp extends Model
{
    use HasFactory;
    protected $table = 'allottment_exp';
    protected $fillable = [
        'id',
        'allotted_by',
        'allotted_to',
        'office_id',
        'office_level',
        'd_no', 
        'major_code', 
        'sub_major_code',   
        'minor_code', 
        'sub_minor_code',
        'object_head', 
        'full_code', 
        'hoa_id', 
        'ddo_code',
        'treasury',     
        'district', 
        'allotted_amount',
        'available_amount', 
        'expenditure_amount',
        'created_at',
        'updated_at'
    ];
}
