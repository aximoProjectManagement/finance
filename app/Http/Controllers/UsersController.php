<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Auth;

class UsersController extends Controller
{
    public function login(LoginRequest $request){
        if(Auth::attempt(["email" => $request->email, "password" => $request->password])){
            $user = Auth::user();
           // dd($user);
            return response()->json([
            'api_token'  => $user->createToken("api_token")->accessToken,
            'token_type' => 'bearer',
            'data'       =>  $user,
            'status'     => 'success',
            ]);
        }else{
           return response()->json([
               'status' => 'error',
               'message' => 'Please enter valid email & password!'
           ]);
        }
    }
}
