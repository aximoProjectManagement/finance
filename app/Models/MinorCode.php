<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SubMajorCode;

class MinorCode extends Model
{
    use HasFactory;
    protected $table = 'minor_code';
    protected $primaryKey = 'id';
    protected $fillable = [
        'sub_major_id',
        'code',
        'description',
    ];
    // public function submajor(){
    //     return $this->belongsTo(SubMajorCode::class);
    // }
}
