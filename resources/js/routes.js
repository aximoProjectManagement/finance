import { defaults } from "lodash";
import VueRouter from "vue-router";

import ExampleComponent from './components/ExampleComponent.vue';
import LoginComponent from './components/LoginComponent.vue';
import PostLoginComponent from './components/PostLoginComponent.vue';
import CodewiseComponent from './components/CodewiseComponent.vue';
import MaintainanceComponent from './components/MaintainanceComponent.vue';
import ProfileComponent from './components/ProfileComponent.vue';
import TeamMemberComponent from './components/TeamMemberComponent.vue';
import OrganizationComponent from './components/OrganizationComponent.vue';
import DesignationComponent from './components/DesignationComponent.vue';
import AnalysisComponent from './components/AnalysisComponent.vue';
import AfterLoginComponent from './components/AfterLoginComponent.vue';
import CreateUserComponent from './components/CreateUserComponent.vue';
import UploadfileComponent from './components/UploadfileComponent.vue';
import ForgetpasswordComponent from './components/ForgetpasswordComponent.vue';
import PowerbiComponent from './components/PowerbiComponent.vue';
import AllotmentEntryComponent from './components/AllotmentEntryComponent.vue';
import UtilizationEntryComponent from './components/UtilizationEntryComponent.vue';
import MasterCorrectionComponent from './components/MasterCorrectionComponent.vue';
import AddMajorCodeComponent from './components/AddMajorCodeComponent.vue';

import AddSubMajorCodeComponent from './components/AddSubMajorCodeComponent.vue';
import AddMinorCodeComponent from './components/AddMinorCodeComponent.vue';
import AddSubMinorCodeComponent from './components/AddSubMinorCodeComponent.vue';
import CustomizedAllotmentComponent from './components/CustomizedAllotmentComponent.vue';
import AllotmentTableComponent from './components/AllotmentTableComponent.vue';
import AllotmentEntryTableComponent from './components/AllotmentEntryTableComponent.vue';
import UtilizationEntryTableComponent from './components/UtilizationEntryTableComponent.vue';
import DistrictwiseComponent from './components/DistrictwiseComponent.vue';
import OfficeWiseComponent from './components/OfficeWiseComponent.vue';




export const router = new VueRouter({
    // mode:'history',
    routes: [{
            path: '/',
            component: LoginComponent,
            name: 'login'
        },
        {
            path: '/index1',
            component: PostLoginComponent,
            name: 'PostLoginComponent'
        },
        {
            path: '/example',
            component: ExampleComponent,
            name: 'ExampleComponent'
        },
        {
            path: '/tab1',
            component: CodewiseComponent,
            name: 'CodewiseComponent'
        },
        {
            path: '/in_progress',
            component: MaintainanceComponent,
            name: 'MaintainanceComponent'
        },
        {
            path: '/profile',
            component: ProfileComponent,
            name: 'ProfileComponent'
        },
        {
            path: '/team_member',
            component: TeamMemberComponent,
            name: 'TeamMemberComponent'
        },
        {
            path: '/organization',
            component: OrganizationComponent,
            name: 'OrganizationComponent'
        },
        {
            path: '/designation',
            component: DesignationComponent,
            name: 'DesignationComponent'
        },
        {
            path: '/analysis',
            component: AnalysisComponent,
            name: 'AnalysisComponent'
        },
        {
            path: '/index',
            component: AfterLoginComponent,
            name: 'AfterLoginComponent'
        },
        {
            path: '/create_user',
            component: CreateUserComponent,
            name: 'CreateUserComponent'
        },
        {
            path: '/upload_file',
            component: UploadfileComponent,
            name: 'UploadfileComponent'
        },
        {
            path: '/forget_password',
            component: ForgetpasswordComponent,
            name: 'ForgetpasswordComponent'
        },
        {
            path: '/power_bi',
            component: PowerbiComponent,
            name: 'PowerbiComponent'
        },
        {
            path: '/allotment_entry',
            component: AllotmentEntryComponent,
            name: 'AllotmentEntryComponent'
        },
        {
            path: '/utilization_entry',
            component: UtilizationEntryComponent,
            name: 'UtilizationEntryComponent'
        },
        {
            path: '/master_correction',
            component: MasterCorrectionComponent,
            name: 'MasterCorrectionComponent'
        },
        {
            path: '/add_major_code',
            component: AddMajorCodeComponent,
            name: 'AddMajorCodeComponent'
        },
        {
            path: '/add_sub_major_code',
            component: AddSubMajorCodeComponent,
            name: 'AddSubMajorCodeComponent'
        },
        {
            path: '/add_minor_code',
            component: AddMinorCodeComponent,
            name: 'AddMinorCodeComponent'
        },
        {
            path: '/add_sub_minor_code',
            component: AddSubMinorCodeComponent,
            name: 'AddSubMinorCodeComponent'
        },
        {
            path: '/customized_allotment',
            component: CustomizedAllotmentComponent,
            name: 'CustomizedAllotmentComponent'
        },
        {
            path: '/allotment_table',
            component: AllotmentTableComponent,
            name: 'AllotmentTableComponent'
        },
        {
            path: '/allotment_entry_table',
            component: AllotmentEntryTableComponent,
            name: 'AllotmentEntryTableComponent'
        },
        {
            path: '/utilization_entry_table',
            component: UtilizationEntryTableComponent,
            name: 'UtilizationEntryTableComponent'
        },
        {
            path: '/district_wise',
            component: DistrictwiseComponent,
            name: 'DistrictwiseComponent'
        },
        {
            path: '/office_wise',
            component: OfficeWiseComponent,
            name: 'OfficeWiseComponent'
        },



    ]
});

export default router;