<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMajorCode extends Model
{
    use HasFactory;
    protected $table = 'sub_major_code';
    protected $fillable = [
        'major_code_id',
        'code',
        'description',
    ];
}
