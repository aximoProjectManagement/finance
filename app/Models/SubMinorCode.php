<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MinorCode;

class SubMinorCode extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table = 'sub_minor_code';
    protected $fillable = [
        'minor_code_id',
        'code',
        'description',
    ];
    public function minorcode(){
        return $this->belongsTo(MinorCode::class,'id');
    }
    public function submajor(){
        return $this->belongsTo(SubMajorCode::class,'id');
    }
    public function majorcode(){
        return $this->belongsTo(MajorCode::class,'id');
    }
}

